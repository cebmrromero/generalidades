# README #

En este archivo se indican todas las generalidades que tenemos que tener en cuenta a la hora de mantener nuestros proyectos

### ¿Para qué es este archivo? ###

* Mantener una normativa global de la forma en la que trabajamos
* Tener un control claro de la documentacion
* Brindar al equipo las herramientas necesarias para unificar criterios

### ¿Cómo escribir en este archivo? ###
* [Leer acerca de Markdown (en inglés)](https://bitbucket.org/tutorials/markdowndemo)

### Acerca de Git ###

* [Pro GIT (Libro)](http://git-scm.com/book/es/v1)
* [Guía rapida de GIF (PDF)](http://practico.sourceforge.net/data/_uploaded/web_img/Git_GuiaRapida.pdf)
* [Guia completa de GIT (en inglés)](https://www.atlassian.com/git/)

### Acerca de BitBucket ###

* [Documentación completa (en inglés)](https://confluence.atlassian.com/x/bgozDQ)
* [Guia de BitBucket (en inglés)](https://confluence.atlassian.com/x/bgozDQ)

### Acerca de los nombres de usuarios ###
* El nombre de usuario estara compuesto por las iniciales de la contraloria, y el nombre de usuario del funcionario desarrollador ejemplo: Contraloria Bolivariana del Estado Mérida (CEBM) y el funcionario Wuilliam Lacruz, usariamos `cebmwlacruz`, por mencionar un ejemplo, de esta forma tenemos claridad de quién esta conectado y haciendo cambios sobre el código.